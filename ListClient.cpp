#include <iostream>
#include "List.h"

using namespace std;

int main()
{
cout << "Welcome to my List ADT client"<<endl;

 List L1,L2 ;//list objects, L1 and L2
 for (int i=0;i<13;i++)
 {
	 L1.insert(i,1);
	 L2.insert(i,1);
 }
 cout << "Please enter data for L1"<<endl;
 cout<< "The size of L1 is "<< L1.size()<<endl;
 cout<<"The size of L2 is "<<L2.size()<<endl;
 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 return 0;
}
